@extends('app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        <h1 class="page-header">
            Halaman 
            <small>Daftar Profile</small>
        </h1>
        <p><a href="{{ route('create') }}" class="btn btn-primary" role="button">Tambah Profile Baru</a></p>
        @if (Session::has('message'))
			{{ Session::get('message') }}
		@endif
		<div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nama</th>
                                        <th>Kelas</th>
                                        <th>Absen</th>
                                        <th>Email</th>
                                        <th>Jenis Kelamin</th>
                                        <th width="146">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($profiles as $value)
                                    <tr>
                                        <td>{{{ $value->id }}}</td>
                                        <td>{{{ $value->nama }}}</td>
                                        <td>{{{ $value->kelas }}}</td>
                                        <td>{{{ $value->absen }}}</td>
                                        <td>{{{ $value->email }}}</td>
                                        <td>{{{ $value->jeniskelamin == 'L' ? 'Laki - laki' : 'Perempuan' }}}</td>
                                        <td>
                                        	<div class="btn-group">
											<a href="{{ URL::to('edit/'.$value->id) }}" class="btn btn-success">Ubah</a>
											<a href="{{ URL::to('crud/destroy/'.$value->id) }}" class="btn btn-danger">Hapus</a>
											</div>
                                        </td>
                                    </tr>
                                @endforeach    
                                </tbody>
                            </table>
            </div>
            {{$profiles->render()}}
</div>
</div>
</div>

@stop