
@extends('app')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        <h1 class="page-header">
            Halaman 
            <small>Tambah Profile</small>
        </h1>

        {!! Form::open(array('urls' => 'create')) !!}
            
            <div class="form-group">
            	{!! Form::label('nama', 'Nama') !!}
		    	{!! Form::text('nama', null, array('class' => 'form-control','placeholder'=>'masukkan nama')) !!}
                {!! '<div>'.$errors->first('nama').'</div>' !!}
            </div>

            <div class="form-group">
                {!! Form::label('kelas', 'Kelas') !!}
                {!! Form::text('kelas', null, array('class' => 'form-control','placeholder'=>'kelas')) !!}
                {!! '<div>'.$errors->first('kelas').'</div>' !!}
            </div>

            <div class="form-group">
                {!! Form::label('absen', 'Absen') !!}
                {!! Form::text('absen', null, array('class' => 'form-control','placeholder'=>'absen')) !!}
                {!! '<div>'.$errors->first('absen').'</div>' !!}
            </div>

            <div class="form-group">
                {!! Form::label('email', 'Email') !!}
                {!! Form::text('email', null, array('class' => 'form-control','placeholder'=>'email')) !!}
                {!! '<div>'.$errors->first('email').'</div>' !!}
            </div>

             <div class="form-group">
               	{!! Form::label('jeniskelamin', 'Jenis Kelamin') !!}
                {!! Form::select('jeniskelamin', array('L' => 'Laki - Laki', 'P' => 'Perempuan'), null, array('class' => 'form-control','placeholder'=>'Pili Jenis Kelamin')) !!}
                {!! '<div>'.$errors->first('jeniskelamin').'</div>' !!}
            </div>

            <div class="form-group">
                {!! Form::label('alamat', 'Alamat') !!}
                {!! Form::textarea('alamat', null, array('class' => 'form-control','placeholder'=>'masukkan alamat')) !!}   
            </div>
            {!! Form::submit('Simpan', array('class' => 'btn btn-primary')) !!}
            <a href="{{ URL::previous() }}" class="btn btn-danger"> Cancel</a>

        {!! Form::close() !!}
        <br />
        <br />
    </div>
</div>
</div>
@stop