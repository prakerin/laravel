@extends('app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        <h3>For Crud <a href=" {{ 'crud' }} " class="btn btn-primary"> crud</a></h3>
        <br />
        <h4>Map Circle <a href=" {{ 'circle' }} " class="btn btn-primary">here</a></h4>
        <h4>Map Click Event <a href=" {{ 'clickevent' }} " class="btn btn-primary">here</a></h4>
        <h4>Map Info <a href=" {{ 'info' }} " class="btn btn-primary">here</a></h4>
        <h4>Map Info Event <a href=" {{ 'infoevent' }} " class="btn btn-primary">here</a></h4>
        <h4>Map Marker <a href=" {{ 'marker' }} " class="btn btn-primary">here</a></h4>
        <h4>Map Poly <a href=" {{ 'poly' }} " class="btn btn-primary">here</a></h4>
        <h4>Map Zoom <a href=" {{ 'zoom' }} " class="btn btn-primary">here</a></h4>
        <h4>Map Database <a href=" {{ 'gmdb' }} " class="btn btn-primary">here</a></h4>
        </div>
    </div>
</div>
@endsection