<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Requests\RegistrationRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use App\User;
use DB;
use View;
use Input;
use Session;
use Validator;

class MapController extends Controller {
	public function insert()
	{			
		DB::table('markers')->insert(
    		array(
	    			'name' => Input::get('name'),
	    			'address' => Input::get('address'),
    			)
			);
		return redirect('gmdb')->withErrors('Data Berhasil Ditambahkan')->withInput();
		}
}