<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Requests\RegistrationRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use App\User;
use DB;
use View;
use Input;
use Session;
use Validator;

class CrudController extends Controller {

	public function index()
	{
		$profiles = DB::table('profiles')->paginate(5);
        return view('crud.index',['profiles' => $profiles]);
    }

	public function create()
	{
		return view('crud.create');
	}

	public function store()
	{
		$rules = array(
			'nama' => 'required',
			'kelas' => 'required',
			'absen' => 'required | numeric',
			'email' => 'required | email | unique:users',
			'jeniskelamin' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {	
			return redirect('create')->withErrors($validator)->withInput();
		} else {			
		DB::table('profiles')->insert(
    		array(
	    			'nama' => Input::get('nama'),
	    			'kelas' => Input::get('kelas'),
	    			'absen' => Input::get('absen'),
	    			'email' => Input::get('email'),
	            	'jeniskelamin' => Input::get('jeniskelamin'),
	            	'alamat' => Input::get('alamat')
    			)
			);
    
       	Session::flash('message', 'Data Berhasil Ditambahkan');
		return Redirect::to('crud');
		}
	}

	public function edit($id)
	{
		$profilesbyid = DB::table('profiles')->where('id',$id)->first();
        return View::make('crud.edit',['profilesbyid' => $profilesbyid]);
	}


	public function update($id)
	{
		$rules = array(
			'nama' => 'required',
			'kelas' => 'required',
			'absen' => 'required | numeric',
			'email' => 'required | email | unique:users',
			'jeniskelamin' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {	
			echo "string";
			return redirect('edit/'.$id)->withErrors($validator)->withInput();
		} else {
		DB::table('profiles')
            ->where('id', $id)
            ->update(array(
            		'nama' => Input::get('nama'),
	    			'kelas' => Input::get('kelas'),
	    			'absen' => Input::get('absen'),
	    			'email' => Input::get('email'),
	            	'jeniskelamin' => Input::get('jeniskelamin'),
	            	'alamat' => Input::get('alamat')
            	));
    
       	Session::flash('message', 'Data Berhasil Diubah');
		return Redirect::to('crud');
		}
	}

	public function destroy($id)
	{
		DB::table('profiles')->where('id', '=', $id)->delete();
		Session::flash('message', 'Data Berhasil Dihapus');
		return Redirect::to('crud');
	}

}