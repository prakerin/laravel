<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', function () {
    return view('welcome');
});
Route::get('home', '\Bestmomo\Scafold\Http\Controllers\HomeController@index');
// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('crud', [
		'as' => 'crud', 'uses' => 'CrudController@index'
	]);
Route::get('create', [
		'as' => 'create', 'uses' => 'CrudController@create'
	]);
Route::get('edit/{id}', [
		'as' => 'edit', 'uses' => 'CrudController@edit'
	]);
Route::post('create', [
		'as' => 'create', 'uses' => 'CrudController@store'
	]);
Route::post('crud/update/{id}', array('as' => 'crud.update', 'uses' => 'CrudController@update'));
Route::get('crud/destroy/{id}','CrudController@destroy');

Route::get('circle', function () {
    return view('map.GMCircle');
});
Route::get('clickevent', function () {
    return view('map.GMClickEvent');
});
Route::get('info', function () {
    return view('map.GMInfo');
});
Route::get('infoevent', function () {
    return view('map.GMInfoEvent');
});
Route::get('marker', function () {
    return view('map.GMMarker');
});
Route::get('poly', function () {
    return view('map.GMPoly');
});
Route::get('zoom', function () {
    return view('map.GMZoom');
});
Route::get('gmdb', function () {
    return view('map.GMDb');
});
Route::post('insert', 'MapController@insert');